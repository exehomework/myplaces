//
//  MainViewController.swift
//  MyPlaces
//
//  Created by admin on 26.03.2020.
//  Copyright © 2020 Andrew Kurdin. All rights reserved.
//

import UIKit

class MainViewController: UITableViewController {

	let restaurantsNames = [
		"Burger Heroes", "Kitchen", "Bonsai", "Дастархан",
		"Индокитай", "X.O", "Балкан Гриль", "Sherlock Holmes",
		"Speak Easy", "Morris Pub", "Вкусные истории",
		"Классик", "Love&Life", "Шок", "Бочка"
//		"Былина ресторан", "Макдоналдс", "City Cafe", "Ресторан Замок Айвенго", "Сказка", "Емеля", "Просто столовая", "Апшерон", "Старый замок", "Тануки", "Вечная пятница", "Гостинный двор", "Триумф", "Казачок", "Якитория", "Fish Point", "PПицца лидер"
	]
	
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1 // метод можно удалить, так как по умолчанию возвращается 1 секция
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
			return restaurantsNames.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

			// чтобы присвоить значение лейблу ячейки
			cell.textLabel?.text = restaurantsNames[indexPath.row]
			cell.imageView?.image = UIImage(named: restaurantsNames[indexPath.row])
			
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
